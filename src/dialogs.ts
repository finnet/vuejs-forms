import ModalCommon from './components/ModalConfirm.vue';

interface ConfirmParam {
    text: string,
    title: string,
    callBackMethods: object,
    btnOk: string,
    btnCancel: string
}

class Confirm {
    text: string;
    title: string;
    btnOk: string;
    btnCancel: string;
    callBackMethods: object;

    constructor() {
        this.text = '';
        this.title = 'Alerta';
        this.btnOk = 'Ok';
        this.btnCancel = 'Cancel';

        this.callBackMethods = {};
    };


    setParams(params: ConfirmParam){
        this.setText(params.text);
        this.setCallBacks(params.callBackMethods);

        if ( typeof params.title == "string"){
            this.setTitle(params.title);
        }

        if ( typeof params.btnOk == "string"){
            this.setBtnOk(params.btnOk);
        }

        if ( typeof params.btnCancel == "string"){
            this.setBtnCancel(params.btnCancel);
        }
    };

    show(parent: Element, params: ConfirmParam) {
        if (typeof params !== "undefined") {
            this.setParams(params);
        }

        let Modal = window['Vue'].extend(ModalCommon);
        let modalElement = document.createElement("DIV");
        parent.appendChild(modalElement);

        let modal = new Modal(
            {
                el: modalElement,
                propsData: {
                    text: this.text,
                    title: this.title,
                    btnOk: this.btnOk,
                    btnCancel: this.btnCancel,
                    callBackMethods: this.callBackMethods
                }
            }
        );
    };

    setTitle(t: string){
        this.title = t;
    }

    setText(t: string) {
        this.text = t;
    };

    setBtnOk(t: string) {
        this.btnOk = t;
    };

    setBtnCancel(t: string) {
        this.btnCancel = t;
    };

	setCallBacks(o: object){
        this.callBackMethods = o;
    };
}

class MsgBox {
    show(s: string) {
        console.log(s);
    };
}

exports.Confirm = Confirm;
exports.MsgBox = MsgBox;
exports.default = function(){
    var Confirm = Confirm;
    var MsgBox = Confirm;
}